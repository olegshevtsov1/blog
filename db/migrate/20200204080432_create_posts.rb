class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.string :title, null: false, default: ""
      t.text :intro, null: false, default: ""
      t.text :article, null: false, default: ""
      t.integer :time, null: false, default: 1

      t.timestamps
    end
  end
end
