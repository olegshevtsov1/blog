# frozen_string_literal: true

class HomeController < BaseController
  def show
  end

  def about
  end
end
