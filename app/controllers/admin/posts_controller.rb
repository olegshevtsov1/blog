# frozen_string_literal: true

class Admin::PostsController < Admin::BaseController
  before_action :post, only: %i[show edit update destroy destroy_image]

  def index
    @posts = Post.all
  end

  def new
    @post = Post.new
  end

  def show
  end

  def edit
  end

  def create
    @post = Post.new(post_params)
    if @post.save
      redirect_to admin_posts_path
    else
      render :new
    end
  end

  def update
    if @post.update(post_params)
      redirect_to admin_posts_path
    else
      render :edit
    end
  end

  def destroy
    flash[:error] = @post.errors.full_messages unless @post.destroy
    redirect_to admin_posts_path
  end

  def destroy_image
    image = ActiveStorage::Attachment.find_by(record_id: params[:id])
    image.purge
    redirect_to edit_admin_post_path(@post)
  end

  private

  def post
    @post ||= Post.find(params[:id])
  end

  def post_params
    params.require(:post).permit(:title, :article, :time, :image)
  end
end
