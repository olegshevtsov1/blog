server 'staging.blog.up.km.ua', user: 'deployer',  roles: %w{web app db}, port: 32015
# set :branch, 'staging'
set :branch, 'task/BLOG-12-resource-posts'
set :rails_env, 'staging'
set :stage, 'staging'
set :sidekiq_env, 'staging'
set :deploy_to, '/home/deployer/apps/staging.blog.up.km.ua'
